// code pour liens sortants:

$(document).ready(function() { 
			
			/* 
			 * 0: js-hidden must be hidden
			 ****************************************************
			 */ 
			 
			$(".js-hidden").hide();
			
			// retrieve hash, as in #reservation
			// info: http://stackoverflow.com/questions/1822598/
			
			var hash = document.URL.substr(document.URL.indexOf('#')+1);
			
			if ((hash == 'reservation') || (hash == 'formulaire-commande')) {
				// alert('works!');
				$("#formulaire-commande").show();
				location.hash = '#formulaire-commande';
			} else {
				// alert(':(');
			}
			
			

			$("a[href^=http]").each(
			   function(){ 
	          if(this.href.indexOf(location.hostname) == -1) {
	      			$(this).attr('target', '_blank');
	    			}
			  	}
			 )
			 
			 
			 // Ancien Site:
			 $("#lien-reservation a").click(function(){
					 	$("#formulaire_contact").slideToggle("slow");
					 	return false;
			 }); 
			 
			 
			 // Nouveau Site:
			 // CLICK sur: a.reservation-btn
			 // AFFICHER : div.formulaire-commande
			 
			 $("#reservation-btn").click(function(){
			 		 	$("#article-content").hide();
			 		 	$("#formulaire-commande").show();
			 		 	// $(document).scrollTop( $("#formulaire-commande").offset().top );
			 		 	location.hash = '#formulaire-commande';
			 		 	// http://stackoverflow.com/questions/3163615/how-to-scroll-html-page-to-given-anchor-using-jquery-or-javascript
			 		 	return false;
			 });
			 
			 
			/* 
			 * 1.
			 * EmailSpamProtection
			 ****************************************************
			 */
			
			$('.spancrypt').empty().append('@');
			$('a.spip_mail').attr('title',function(i, val) {	return val.replace(/\.\..t\.\./g,'@');	});
			
			
			// Source: http://liquidslider.kevinbatdorf.com/tutorials/hash-link-multiple-sliders/#sthash.eO7bBow8.dpuf
			
				/* If you want to adjust the settings, you set an option
				         as follows:
				
				          $('#slider-id').liquidSlider({
				            autoSlide:false,
				            autoHeight:false
				          });
				
				         Find more options at http://liquidslider.kevinbatdorf.com/
				*/
				
				// Initialize two sliders
				  $('#slider-id').liquidSlider();
				  $('#slider-et-encore').liquidSlider();
				  
				  // Store each slider as an object to access internal properties
				  var sliderObject = $.data( $('#slider-id')[0], 'liquidSlider');
				  var sliderObject2 = $.data( $('#slider-et-encore')[0], 'liquidSlider');

				// other stuff removed into liquid.js
				
				// Note: breaks anything that happens after...
				// when using spip's JQ 1.7.
				
	
				
// end (document).ready
});


