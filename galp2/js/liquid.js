// Initialize two sliders
  $('#slider-id').liquidSlider();
  $('#slider-et-encore').liquidSlider();
  
  // Store each slider as an object to access internal properties
  var sliderObject = $.data( $('#slider-id')[0], 'liquidSlider');
  var sliderObject2 = $.data( $('#slider-et-encore')[0], 'liquidSlider');



				  // Check if a hash tag exists
				  if (window.location.hash) {
				    // Cycle through each slider to find which 
				    // tab the hash link refers to.
				    sliderObject.clickable = 'true';
				    sliderObject.options.hashLinking = 'true';
				    sliderObject.getHashTags(window.location.hash);
				    if (typeof sliderObject.hashValue === 'number') {
				      sliderObject.currentTab = sliderObject.hashValue - ~~(sliderObject.options.continuous);
				      sliderObject.setCurrent();
				    } else {
				      // Try the second slider if not found in the first.
				      sliderObject2.clickable = 'true';
				      sliderObject2.options.hashLinking = 'true';
				      sliderObject2.getHashTags(window.location.hash);
				      if (typeof sliderObject2.hashValue === 'number') {
				        sliderObject2.currentTab = sliderObject2.hashValue - ~~(sliderObject2.options.continuous);
				        sliderObject2.setCurrent();
				      } else {
				        
				      }
				    }
				  }
				  $('#slider-1-wrapper a').on('click', function() {
				    sliderObject.options.hashLinking = 'true';
				  });
				  $('#slider-2-wrapper a').on('click', function() {
				    sliderObject2.options.hashLinking = 'true';
				  });

				