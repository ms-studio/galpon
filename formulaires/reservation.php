<?php
 
 // partie 1 = charger
 
function formulaires_reservation_charger_dist($id_article,$titre){
		
		$valeurs = array(
			'nom'		=>	'',
			'prenom'		=>	'',
			'email'		=>	'',
			'telephone' => '',
			'spectacles' => '',
			'nombre_de_places' => '',
			'nom_spectacle' => '',
			'commentaire' => '',
			'abo_newsletter' => '',
			'id_article' => $id_article,
			'titre' => $titre,
			);
		if (isset($GLOBALS['visiteur_session']['email'])) {
		        $valeurs['email'] = $GLOBALS['visiteur_session']['email'];
		}
		return $valeurs;
}

	// partie 2 = verifier

function formulaires_reservation_verifier_dist(){
        $erreurs = array();
        // verifier que les champs obligatoires sont bien la :
        foreach(array('nom','prenom','email','telephone','spectacles','nombre_de_places') as $obligatoire)
                if (!_request($obligatoire)) $erreurs[$obligatoire] = 'Ce champ est obligatoire';
       
        // verifier que si un email a été saisi, il est bien valide :
        include_spip('inc/filtres');
        if (_request('email') AND !email_valide(_request('email')))
                $erreurs['email'] = 'Cet email n\'est pas valide';
 
        if (count($erreurs))
                $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';
        return $erreurs;
}

	// partie 3 = traiter

function formulaires_reservation_traiter_dist(){

		$envoyer_mail = charger_fonction('envoyer_mail','inc');
		
		$subscribe = charger_fonction('subscribe','newsletter');
		
		
		
		// DESTINATAIRE:
		$email_public = _request('email');
		$email_interne = 'reservation@galpon.ch'; // was: billet@
		
		// EXPEDITEUR:
		$email_from = 'reservation@galpon.ch'; // overriden by message_array
		$email_interne_from = 'webmaster@galpon.ch'; // overriden by message_interne
		
		// SUJET:
		$nom_spectacle_propre = strip_tags(_request('nom_spectacle'));
		$nom_spectacle_propre = str_replace("&shy;","",$nom_spectacle_propre);
		$sujet = 'Réservation de Spectacle – Théâtre du Galpon';
		$sujet_interne = 'Réservation Galpon: '.$nom_spectacle_propre;
		
		// CORPS DU MESSAGE:
		// non-utilisé, pour référence...
		$message = 'Coordonnées '."\n\n".'E-mail: '._request('email')."\n".'Téléphone: '._request('telephone')."\n\n".'Détails de la réservation'."\n\n".'Spectacle: '._request('nom_spectacle').'Date: '._request('spectacles')."\n".'Nombre de places: '._request('nombre_de_places');
		
		$message_array = array();
		$message_interne = array();
		
		if (_request('abo_newsletter') == 'on') {
			$ajout_newsletter = 'oui';
		} else {
			$ajout_newsletter = 'non';
		}
		
		/*
		 * Message pour le public - texte
		************************************/
		         
		$message_array['texte'] = '**Détails de la réservation**
		
		Spectacle: '.$nom_spectacle_propre.'
		Date: '._request('spectacles').'
		Nombre de places: '._request('nombre_de_places').'
		
		**Coordonnées** 
		
		Vous nous avez communiqué les coordonnées suivantes:
		Nom: '._request('nom').'
		Prénom: '._request('prenom').'
		E-mail: '._request('email').'
		Téléphone: '._request('telephone').'
		
		**Autres informations**
		
		Les réservations se font au plus tard 2 heures avant le début de l’événement (paiement en espèces uniquement).
		
		Les billets sont à retirer au plus tard 20 minutes avant la représentation directement au Galpon.
		
		--
		**Le Théâtre du Galpon**
		maison pour le travail des arts de la scène
		au pied du bois de la Bâtie, sur les bords de l’Arve
		2, route des Péniches
		case postale 100 - 1211 Genève 8
		T. +41 22 321 21 76
		www.galpon.ch | contact@galpon.ch
		';
		
		/*
		 * Message pour le public - HTML
		************************************/
		
		$message_array['html'] = '<html>
		<head>
		<title>'.$sujet.'</title>
		</head>
		<body> 
		<h1>Informations de réservation</h1> 
		
		<h2>Détails de la réservation</h2> 
		<p><b>Spectacle:</b> '.$nom_spectacle_propre.'<br />
		<b>Date:</b> '._request('spectacles').'<br/>
		<b>Nombre de places:</b> '._request('nombre_de_places').'</p>
		
		<h2>Coordonnées</h2>
		<p>Vous nous avez communiqué les coordonnées suivantes:</p>
		<p><b>Nom:</b> '._request('nom').'<br />
		<b>Prénom:</b> '._request('prenom').'<br />
		<b>E-mail:</b> '._request('email').'<br />
		<b>Téléphone:</b> '._request('telephone').'</p> 
		
		<h2>Autres informations</h2>
		<p>Les <b>réservations</b> se font au plus tard <b>2 heures avant le début de l’événement</b> (paiement en espèces uniquement).</p>
		<p><b>Les billets</b> sont à retirer au plus tard <b>20 minutes</b> avant la représentation directement au Galpon.</p>
		<p style="font-size:90%;">
		<br />
		<span style="color:#f60037">Le Théâtre du Galpon – Genève</span><br />
		maison pour le travail des arts de la scène<br />
		au pied du bois de la Bâtie, sur les bords de l’Arve<br />
		2, route des Péniches<br />
		case postale 100 - 1211 Genève 8<br />
		T. +41 22 321 21 76<br />
		<a href="http://www.galpon.ch/" style="color:#f60037">www.galpon.ch</a> | <a href="mailto:contact@galpon.ch" style="color:#f60037">contact@galpon.ch</a></p>
		</body></html>';
		
		/*
		 * Message pour l'administrateur - texte
		******************************************/
		
		$message_interne['texte'] = '
		Détails de la réservation:
		
		**Coordonnées**
		Nom: '._request('nom').'
		Prénom: '._request('prenom').'
		E-mail: '._request('email').'
		Téléphone: '._request('telephone').'
		
		**Informations de réservation**
		Spectacle: '.$nom_spectacle_propre.'
		Date: '._request('spectacles').'
		Nombre de places: '._request('nombre_de_places').'
		
		**Fin du message**';
		
		/*
		 * Message pour l'administrateur - HTML
		******************************************/
		
		$message_interne['html'] = '<html>
		<head>
		<title>'.$sujet_interne.'</title>
		</head>
		<body> 
		<h1>Informations de réservation</h1> 
		<h2>Coordonnées</h2>
		<p><b>Nom:</b> '._request('nom').'<br />
		<b>Prénom:</b> '._request('prenom').'<br />
		<b>E-mail:</b> '._request('email').'<br />
		<b>Téléphone:</b> '._request('telephone').'<br />
		<b>Newsletter:</b> '.$ajout_newsletter.'
		</p> 
		<h2>Spectacle</h2> 
		<p><b>Nom:</b> '.$nom_spectacle_propre.'<br />
		<b>Date:</b> '._request('spectacles').'<br/>
		<b>Nombre de places:</b> '._request('nombre_de_places').'</p>
		
		</body></html>';
		   
		// EXPEDITEUR (-> public)   
		$message_array['nom_envoyeur'] = "Billetterie du Galpon";
		$message_array['from'] = 'reservation@galpon.ch';
		
		// EXPEDITEUR (-> interne)
		$message_interne['nom_envoyeur'] = _request('prenom') .' '. _request('nom');
		$message_interne['from'] = _request('email'); // prioritaire sur argument $from de premier niveau
		//$message_interne['bcc'] = 'code@ms-studio.net';
		// $message_interne['cc'] = 'contact@galpon.ch';
		$message_interne['headers'] = array(
			'Sender: Billetterie du Galpon <reservation@galpon.ch>', 
			// $item2, ...
		);
		
		// Envoi au public
		// Désactivé sur demande (email du 9 sept. 2013)
		
		//$envoyer_mail(
		//	$email_public,
		//	$sujet,
		//	$message_array,
		//	$email_from
		//);
		
		
		// Envoi à l'administrateur
		
		$envoyer_mail(
			$email_interne, // string $destinataire
			$sujet_interne, // string $sujet
			$message_interne, // string/array $corps
			$email_interne_from // $string $from - DEPRECIE
		);
		
		
		// Ajout à la newsletter
		
		if (_request('abo_newsletter') == 'on') {
			$options_newsletter = array(
				 'nom' => _request('prenom') .' '. _request('nom'),
				 'listes' => array('newsletter_galpon'),
				 'force' => true,
			);
			$email_newsletter = _request('email');
			$subscribe($email_newsletter, $options_newsletter);
		}
		
		// documentation API newsletter: 
		// http://contrib.spip.net/API-Newsletter?lang=fr
		
		
		return array('message_ok'=>"Votre réservation nous a été transmise.<br/>Vous recevrez un message de confirmation dès que votre réservation sera validée.");

}

// fin de la fonction Traiter_Dist

// src: 
// http://www.spip.net/fr_article3796.html
// http://www.mail-archive.com/spip@rezo.net/msg09580.html 

// http://code.spip.net/autodoc//tree/ecrire/inc/envoyer_mail.php.html#f_inc_envoyer_mail_dist
 
// About the Reply-To:
 
 
?>
